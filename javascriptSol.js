// 1.  Write a function which take a number `n` as an argument and returns n'th prime number.

//         - example
//           n = 2 --> 3
//           n = 5 --> 11
//           n = 1 --> 2

    const findnthPrime = num => {
    let i, primes = [2, 3], n = 5;
    const isPrime = n => {
    let i = 1, p = primes[i],
    limit = Math.ceil(Math.sqrt(n));
    while (p <= limit) {
    if (n % p === 0) {
    return false;
    }
    i += 1;
    p = primes[i];
    }
    return true;
    }
    for (i = 2; i <= num; i += 1) {
    while (!isPrime(n)) {
    n += 2;
    }
    primes.push(n);
    n += 2;
    };
    return primes[num - 1];
    }
    console.log(findnthPrime(2));
    console.log(findnthPrime(5));
    console.log(findnthPrime(1));

// 2.  Write a function which a string `str` as an argument and returns the number of time each unique letter repeated.

//         - example
//           str = "ashish" --> a-1, s-2, h-2, i-1
//           str = "lavanya" --> l-1, a-3, v-1, n-1, y-1
//           str = "sreekanth" --> s-1, r-1, e-2, k-1, a-1, n-1, t-1, h-1

// 
function count_numberofoccur( str ){
  for( let i = 0 ;i < str.length ;i++)
  {
    let count = 0;
    for( let j = 0 ;j < str.length ;j++)
    {
    if( str[i] == str[j] && i > j )
    {
    break;
    }
    if( str[i] == str[j] )
    {
      count++;
    }
    }
    if( count > 0)
    console.log(`${str[i]} -> ${count}`);
    
  }
  
  }
  let str = "ashish";
  count_numberofoccur(str);

// 3. Write a function which a string `str` as an argument and returns all the vowels in the string and total number of vowels.

//     - example
//       str = "we are good devs" --> e,a,e,o,o,e - 3.
//       str = "ya we really are" --> a,e,e,a,a,e - 2.
//       ste = "are we really ?" --> a,e,e,e,a - 2.
    countvowels = str => str.match(/([aeiou])(?!.*\1)/gi).join('');
    console.log(countvowels("ya we really are").length)

// 4. Write a function which take a number `n` as an argument and return n length fibonecci series till

//     - example
//       n = 4 ---> 0, 1, 1, 2
//       n = 5 ---> 0, 1, 1, 2, 3
//       n = 6 ---> 0, 1, 1, 2, 3, 5

function nthfibonacci(n) {
var answer = [];
var x = 0;
var y = 1;
var z;
answer.push(x);
answer.push(y);

var i = 2;
while (i < n) {
z = x + y;
x = y;
y = z;
answer.push(z);
i = i + 1;
}
return answer;
}
answer = nthfibonacci(4);
console.log(answer);

// 5.  Write a function which takes a number `n` as an argument and returns its factorial

//     - example (`!` represents factorial) (`n!` = n _ n-1 _ n-2 _ n-3 _ ..... \* 1 )
//       3! ---> 6  
//        4! ---> 24
//       5! ---> 120

function factorial(n) {
if (n == 0) return 1;
return n * factorial(n - 1);
}
console.log(factorial(3));

// 6.  Write a function which takes a number `n` and prints if its a prime or not.

// - example
//   2 ---> prime number
//   4 ---> not a prime number
//   5 ---> prime number

function prime_test(n) {

            var n, i, flag = true;
            for(i = 2; i <= n - 1; i++)
                if (n % i == 0) {
                    flag = false;
                    break;
                }
            if (flag == true)
                console.log('prime number');
            else
                console.log('not a prime number');
        }

prime_test(12)

7. Write a function which takes to arguments `n`, `m` (numbers) and return an array of even numbers between `n` & `m`

- example
  2, 10 ---> [2, 4, 6, 8, 10]
  3, 9 ---> [4, 6, 8]
  110, 115 ---> [110, 112, 114]
  function evenNumbersinrange(minNumber, maxNumber){
    let str = '';
    for (let i = Math.ceil((minNumber + 0.5) / 2) * 2; i <= maxNumber; i += 2) {
      str += `${i},`;
    }
    return str.slice(0,-1);
  }
  
  console.log(evenNumbersinrange(3,9));

// 8. Write a functions which takes an array of string as an argument and return sorted array

// - example
//   ['a', 'd', 'c', 'b'] ---> ['a', 'b', 'c', 'd']
//   ['g','f','a', 'd', 'c', 'b'] ---> ['a', 'b', 'c', 'd', 'f', 'g']
//   ['a', 's', 'u', 'd', 'c', 'b'] ---> ['a', 'b', 'c', 'd', 's', 'u']


function sort(arr) {
	console.log(arr.sort());
}
sort(['a', 's', 'u', 'd', 'c', 'b']);
or
var arr = ['a', 's', 'u', 'd', 'c', 'b'];
arr.sort()
console.log(arr)

// 9. Write a function which takes an object as an argument and return a object with all `null` replaced with empty string

// - example
//   {a: null} ---> {a: ''}
//   {a: null, b: undefined} ---> {a: '', b: undefined}
//   {a: null, b: undefined, c: 1} ---> {a: '', b: undefined, c:1}

let obj = {a: null, b: undefined, c: 1};
let result = Object.keys(obj).reduce((acc, b) => { acc[b] = obj[b] === null ? '' : obj[b]; return acc},{})
console.log(result)



// 10. Write a function which takes an array of numbers and return the sum of the numbers

// - example
//   [1, 10, 100, 100] ---> 1111
//   [10, 100, 4] ---> 114
//   [-50, 50, 0, 200] ---> 200

  function accumulateSum(arr, n)
  {
  let sum = 0;
  for(let i = 0; i < n; i++)
  {
  sum = sum + arr[i];
  }
  return sum;
  }
  let arr = [-50, 50, 0, 200];
  let n = arr.length;
  console.log(accumulateSum(arr, n));

// 11. Write a function which takes a string as an argument and return a Boolean if the given string is a palindrome or not.
//     Note: Donot use any inbuilt methods
//     str = 'liril' ---> true
//     str = 'kiran' ---> false

function checkPalind(str) {
const len = str.length;
for (let i = 0; i < len / 2; i++) {
if (str[i] !== str[len - 1 - i]) {
return 'false';
}
}
return 'true';
}
const str = 'liril';
const value = checkPalind(str);
console.log(value);

// 12. Write a function which takes an array of numbers as a argument and a number as a second argument.
//     The function should return array of numbers which are greater than second argument.

// - example

//   ([5, 6, 1, 7, 90, 23, 4], 10) ---> [90, 23]

  function filterGreaternumbers(arr, num) {

  return arr.filter((item) => {
  return item > num
  })
  }
  console.log(filterGreaternumbers([5, 6, 1, 7, 90, 23, 4], 10))

// 13. Write a function which takes an array of numbers as a argument and it should remove the duplicates from the array.

// - example
//   [5, 5, 4, 3, 4] ---> [5, 4, 3]

  function filterDuplicates(array) {
  return array.filter((element, index) => array.indexOf(element) === index);
  }
  console.log(filterDuplicates([5, 5, 4, 3, 4]))

// 14. Write a function which takes an object an argument with key value pairs (value should be a number) and return the sum of all values

// - example
//   {
//   price: 300,
//   tax: 100,
//   profit: 50
//   } ---> 450

  function objectSumfunction(a) {
  return Object.values(a).reduce((sum, cur) => sum + cur, 0);
  }
  console.log(objectSumfunction({
  price: 300,
  tax: 100,
  profit: 50
  }))

// 15. Let's say I have two objects
//     const carDetails = {
//     brand: 'Volvo',
//     year: 1970,
//     country: 'Sweedon'
//     }
//     const dealer = {
//     dealerName: 'Cars and Trucks',
//     dealerCity: 'Hyderabad',
//     dealerRating: 5
//     }
//     I want you to copy the carDetails to dealer object.

const carDetails = {
  brand: 'Volvo',
  year: 1970,
  country: 'Sweedon'
  };
  const dealer = {
  dealerName: 'Cars and Trucks',
  dealerCity: 'Hyderabad',
  dealerRating: 5
  };

Object.assign(dealer ,carDetails)

console.log(dealer)

  dealer -
  {
    dealerName: 'Cars and Trucks',
    dealerCity: 'Hyderabad',
    dealerRating: 5,
    brand: 'Volvo',
    year: 1970,
    country: 'Sweedon'
  }
